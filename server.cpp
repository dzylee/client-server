/*
Zeng-Yuan Lee
Assignment 4, Problem 1, Question 1
CMPT 471
Spring 2016
*/

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//                           THE ROTOR MACHINE
/*
This simulation of a rotor-based encryption/descryption machine has:

m=67 symbols. The symbols are:
    ABCDEFGHIJKLMNOPQRSTUVWXYZ //26
    abcdefghijklmnopqrstuvwxyz //26
    0123456789                 //10
     ,.?!                      //5
                               //Total: 67

r=2 rotors. This program actually provides 3 different rotors to choose from,
but only two rotors are ever use at any a given time. In this program, each
rotor has a fixed one-to-one mapping, which means that a rotor and a
permutation are the same thing. Since there are three different rotors to
choose from, it implies that there three different one-to-one mappings of
input pins to output pins (i.e. permutations) to choose from.

N=6 schemes. Since this program offers three different rotors to choose from,
there is a total of 3!=6 schemes to choose from.
*/
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


/*
This integer constant indicates how many symbols, how many input pins, and how
many output pins each rotor has.
*/
const int SYM_COUNT = 67;

/*
Finds the first occurrence of an integer in an array of integers, and returns
the index of that integer. If the integer being searched for is not in the
array, the function returns -1. The first parameter is used to specify the
integer you are looking for, and the second parameter is used to specify the
array to search in.
*/
int indexOfIntInArr(int val, int* intArr)
{
    int index = -1;

    for( int i=0; i<SYM_COUNT; i++ )
    {
        if( val == intArr[i] )
        {
            index = i;
            break;
        }
    }
    return index;
}

/*
Finds the first occurrence of an char in an array of chars, and returns the
index of that char. The first parameter is used to specify the char you are
looking for, and the second parameter is used to specify the array of chars to
search in.
*/
int indexOfCharInArr(char val, char* charArr)
{
    char* ptrSymbol = strchr(charArr, val);
    return (ptrSymbol - charArr);
}

/*
Defines a struct data type which is used to represent a rotor. The number of
input pins and output pins is set to the value in the integer constant
SYM_COUNT (defined at the beginning of this file).
*/
struct rotor
{
    int ipins[SYM_COUNT]; //Input pins.
    int opins[SYM_COUNT]; //Output pins.
    //int* ptrCurrentPin;
};

/*
Turns a rotor FORWARD by a specified number of positions (pins). The first
parameter is the rotor you want to turn (created using the rotor struct
datatype) and the second parameter is an integer that specifies how many pin
positions you want to turn the rotor by.
*/
void turnRotor(rotor* ptrRotor, int numTurns)
{
    if( numTurns >= 0 )
    {
        numTurns = numTurns % SYM_COUNT;
        rotor tempRotor;

        for( int i=numTurns, j=0; i<SYM_COUNT; i++, j++ )
        {
            tempRotor.ipins[j] = ptrRotor->ipins[i];
            tempRotor.opins[j] = ptrRotor->opins[i];
        }

        for( int i=numTurns-1, j=SYM_COUNT-1; i>=0; i--, j-- )
        {
            tempRotor.ipins[j] = ptrRotor->ipins[i];
            tempRotor.opins[j] = ptrRotor->opins[i];
        }

        for( int i=0; i<SYM_COUNT; i++ )
        {
            ptrRotor->ipins[i] = tempRotor.ipins[i];
            ptrRotor->opins[i] = tempRotor.opins[i];
        }
    }
    return;
}

/*
Turns a rotor BACKWARD by a specified number of positions (pins). The first
parameter is the rotor you want to turn (created using the rotor struct
datatype) and the second parameter is an integer that specifies how many pin
positions you want to turn the rotor by.
*/
void turnRotorRev(rotor* ptrRotor, int numTurns)
{
    if( numTurns >= 0 )
    {
        numTurns = numTurns % SYM_COUNT;
        rotor tempRotor;

        for( int i=SYM_COUNT-1, j=numTurns-1; j>=0; i--, j-- )
        {
            tempRotor.ipins[j] = ptrRotor->ipins[i];
            tempRotor.opins[j] = ptrRotor->opins[i];
        }

        for( int i=SYM_COUNT-numTurns-1, j=SYM_COUNT-1; i>=0; i--, j-- )
        {
            tempRotor.ipins[j] = ptrRotor->ipins[i];
            tempRotor.opins[j] = ptrRotor->opins[i];
        }

        for( int i=0; i<SYM_COUNT; i++ )
        {
            ptrRotor->ipins[i] = tempRotor.ipins[i];
            ptrRotor->opins[i] = tempRotor.opins[i];
        }
    }
    return;
}


/*
The 67 symbols that the imaginery keyboard supports. Each symbol in this array
is "connected to" the input value with the SAME INDEX NUMBER, in the FIRST
rotor. The first rotor is whichever rotor is chosen to be the first.
*/
char keyboard[] =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ,.?!";

/*
The three one-to-one mappings (i.e. permutations) that our rotor machine can
choose from. For our purposes, each rotor has a fixed one-to-one mapping (i.e.
each rotor's permutation is fixed). Therefore, since there are three rotors
(A, B, and C) defined here, it implies there are three permutations to choose
from.
*/
                // A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  0  1  2  3  4  5  6  7  8  9  _  ,  .  ?  !
rotor rotorA = { {61,10,17,18,58,67,56,57, 2,42,21, 8,19,32, 4,47,49,62,34,31,22,53, 7,15,28,39,48,20,44, 6, 3,63,36,60,66,52,64, 9,14,13,38,40, 1,46,30,51,65,33,25,37,59,54,24,16,23, 5,29,50,45,35,27,41,26,12,11,43,55},
                 {50,19,27,53,65,29,17, 9,66,39,62,40,32,25,23,56,24,48,11,21,37,55,67,41,18, 2,14,36,61,16, 6, 7,60,38,42,46, 4,20, 5,30,33,13,31,54,58,10,43,35,51,26,57,22,47,64,45,63, 3,12,49,34,44,15,59, 1, 8,28,52}
               };

rotor rotorB = { { 4,23,25,39,15, 6,24,50,54, 3,56,43,63,41,18,42,20,49,32,17,46,66,61,38,13,45,12, 2,35,27,30,65,28,37,29,62,22,26,31,60,44, 1,67,48,36, 8, 7,16, 9,40,59, 5,57,19,21,11,58,14,33,52,34,47,51,64,53,55,10},
                 { 3,18, 8,47,12,35,25,43,24,45,31,48,39,23,60, 5,66, 1,28,26,14,65,49, 7,62,21,53,16,64,34,13,63,36,59,58,51,67,30,55,40,38, 9,32,17,33,11, 6,56,54,46,44, 4,50,61,15,20,41,42,57,27,19, 2,29,52,10,37,22}
               };

rotor rotorC = { {24,23,40, 1,20,31,44,41,43, 6,50, 4,49,57,34,29,42,35,56, 2,59,58,10,47,21,53,54,46,15,19,61,60,62,28,12,30,36,66, 3,13,65,16,64,37,25,45,48, 7,14, 8, 5,63,33,52,18,22, 9,17,55,32,39,27,26,51,11,38,67},
                 {43,11,19, 6,20,65,34,31,39,40,27,26,55,60,57,28,46,42,22, 3,50,38,52,41,61,66,13,53,58,32,44, 7,54,18,29,64, 1,15, 4,47,33,16,59,56,17,12, 2,23,21,35, 5,63,30,62,67, 9,36,10,48,45,25, 8,37,14,24,51,49}
                // A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  0  1  2  3  4  5  6  7  8  9  _  ,  .  ?  !
               };

/*
This array is identical to the keyboard[] char array. Each symbol in this array
is "connected to" the output value with the SAME INDEX NUMBER, in the LAST
rotor. The last rotor is whichever rotor is chosen to be the last.
*/
char encOutput[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ,.?!";


/*
Encrypts a plaintext message and returns a pointer to the char array that holds
the encrypted result. The first parameter is a char array that represents a
C-style string that holds the plaintext message. The second and third
parameters are the rotors you want to use as the first and second rotors,
respectively, in the encryption machine. The rotors are created using the
rotor struct datatype.
*/
char* enc(char* plaintext, rotor c1, rotor c2) //plaintext is a C-style string.
{
    int msglen = strlen(plaintext);         //strlen() excludes the terminating '\0' in its count.

    int index;                              //Used to temporarily store index positions in an array.
    char* ptrC2output = new char[msglen+1]; //To hold the final output (i.e. the ciphertext).

    int nTurnsC1 = 0;                       //To keep track of the current pin position on the first rotor.
    int nTurnsC2 = 0;                       //To keep track of the current pin position on the second rotor.

    //Encrypt the message one character at a time:
    for( int i=0; i<msglen; i++ )
    {
        index = indexOfCharInArr( plaintext[i], keyboard ); //Keyboard input.

        //Get the output of the first rotor (C1):
        int c1_inputValue = c1.ipins[index];                //Get the input value on first rotor that is mapped to the keyboard key.
        index = indexOfIntInArr( c1_inputValue, c1.opins ); //Get the index of the output value on the first rotor that is mapped to the input value of the first rotor.

        //Get the output of the second rotor (C2):
        int c2_inputValue = c2.ipins[index];                //Get the input value on second rotor that is mapped to the output value of the first rotor.
        index = indexOfIntInArr( c2_inputValue, c2.opins ); //Get the index of the output value on the second rotor that is mapped to the input value of the second rotor.
        ptrC2output[i] = encOutput[index];                  //Store the encrypted character.

        //Prepare to encrypt the next character by turning the rotors:
        turnRotor(&c1, 1);                  //Turn the first rotor forward by one pin position.
        ++nTurnsC1;
        if( nTurnsC1 == SYM_COUNT )         //Check if the first rotor has made a full rotation.
        {
            nTurnsC1 = 0;
            turnRotor(&c2, 1);
            ++nTurnsC2;
        }
        if( nTurnsC2 == SYM_COUNT )         //Check if the second rotor has made a full rotation.
        {
            nTurnsC2 = 0;
        }
    }
    ptrC2output[msglen] = '\0';             //Turn the array holding the ciphertext into a C-style string.

    return ptrC2output;
}

/*
Decrypts a ciphertext message and returns a pointer to the char array that
holds the decrypted result. The first parameter is a char array that
represents a C-style string that holds the ciphertext message. The second and
third parameters are the rotors you want to use as the first and second
rotors, respectively, in the encryption machine. They need to be the same
rotors that were used to encrypt the original plaintext message. The rotors
are created using the rotor struct datatype.
*/
char* dec(char* ciphertext, rotor c1, rotor c2) //ciphertext is a C-style string.
{
    int msglen = strlen(ciphertext);         //strlen() excludes the terminating '\0' in its count.

    int nTurnsC1 = (msglen-1) % SYM_COUNT;   //To keep track of the current pin position on the first rotor.
    int nFullRotC1 = (msglen-1) / SYM_COUNT; //Calculate the number of full rotations that the first rotor made to encrypt the original plaintext message.
    int nTurnsC2 = nFullRotC1 % SYM_COUNT;   //To keep track of the current pin position on the second rotor.
    turnRotor(&c1, nTurnsC1);                //Turn the first rotor to the position it was in right after it encrypted the original plaintext message.
    turnRotor(&c2, nTurnsC2);                //Turn the second rotor to the position it was in right after it encrypted the original plaintext message.

    int index;                               //Used to temporarily store index positions in an array.
    char* ptrC1input = new char[msglen+1];   //To hold the original input to c1 (i.e. the plaintext).

    //Decrypt the message one character at a time:
    for( int i=msglen-1; i>=0; i-- )
    {
        index = indexOfCharInArr( ciphertext[i], encOutput ); //Ciphertext input.

        //Get the original input to the second rotor (C2):
        int c2_outputValue = c2.opins[index];                 //Get the output value on the second rotor that is mapped to the keyboard key.
        index = indexOfIntInArr( c2_outputValue, c2.ipins );  //Get the index of the input value on the second rotor that is mapped to the output value of the second rotor.

        //Get the original input to the first rotor (C1):
        int c1_outputValue = c1.opins[index];                 //Get the output value on first rotor that is mapped to the input value of the second rotor.
        index = indexOfIntInArr( c1_outputValue, c1.ipins );  //Get the index of the input value on the first rotor that is mapped to the output value of the first rotor.
        ptrC1input[i] = keyboard[index];                      //Store the decrypted character.

        //Prepare to decrypt the next character by turning the rotors:
        turnRotorRev(&c1, 1);                //Turn the first rotor backward by one pin position.
        --nTurnsC1;
        if( nTurnsC1 == -1 )                 //Check if the first rotor has made a full rotation.
        {
            nTurnsC1 = SYM_COUNT-1;
            turnRotorRev(&c2, 1);
            --nTurnsC2;
        }
        if( nTurnsC2 == -1 )                 //Check if the second rotor has made a full rotation.
        {
            nTurnsC2 = SYM_COUNT-1;
        }
    }
    ptrC1input[msglen] = '\0';               //Turn the array holding the plaintext into a C-style string.

    return ptrC1input;
}

/*
This function simulates the encryption machine. It is used to encrypt or
decrypt a message using a specified scheme, and then return a pointer to a
character array that contains the result.

The first parameter is used to specify the scheme to use. A scheme determines
which rotors are used as the first and second rotors. These are 6 schemes,
along with the first and second rotors (from left to right) that each scheme
uses:

scheme 0: rotors A,B
scheme 1: rotors A,C
scheme 2: rotors B,A
scheme 3: rotors B,C
scheme 4: rotors C,A
scheme 5: rotors C,B

The second parameter, encdec, is an integer value used to select whether to
encrypt or decrypt:

0 encrypts
1 decrypts

The third parameter is a C-style string (an array of chars) that represents
either a plaintext or a ciphertext.
*/
char* scheme(int schemeNum, int encdec, char* messageStr)
{
    if(encdec == 0) //Encrypt
    {
        char* ciphertext;
        switch(schemeNum)
        {
            case 0:  ciphertext = enc(messageStr, rotorA, rotorB);
                     return ciphertext;
                     break;
            case 1:  ciphertext = enc(messageStr, rotorA, rotorC);
                     return ciphertext;
                     break;
            case 2:  ciphertext = enc(messageStr, rotorB, rotorA);
                     return ciphertext;
                     break;
            case 3:  ciphertext = enc(messageStr, rotorB, rotorC);
                     return ciphertext;
                     break;
            case 4:  ciphertext = enc(messageStr, rotorC, rotorA);
                     return ciphertext;
                     break;
            case 5:  ciphertext = enc(messageStr, rotorC, rotorB);
                     return ciphertext;
                     break;
            default: break;
        }
    }

    if(encdec == 1) //Decrypt
    {
        char* plaintext;
        switch(schemeNum)
        {
            case 0:  plaintext = dec(messageStr, rotorA, rotorB);
                     return plaintext;
                     break;
            case 1:  plaintext = dec(messageStr, rotorA, rotorC);
                     return plaintext;
                     break;
            case 2:  plaintext = dec(messageStr, rotorB, rotorA);
                     return plaintext;
                     break;
            case 3:  plaintext = dec(messageStr, rotorB, rotorC);
                     return plaintext;
                     break;
            case 4:  plaintext = dec(messageStr, rotorC, rotorA);
                     return plaintext;
                     break;
            case 5:  plaintext = dec(messageStr, rotorC, rotorB);
                     return plaintext;
                     break;
            default: break;
        }
    }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//                            THE SERVER PROGRAM
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

int main(void)
{
    cout << endl << "RESETTING NETWORK INTERFACE \'eth1\'. WAIT HERE..." << endl << endl;
    system("sudo ifconfig eth1 down; sudo ifconfig eth1 up; sudo ifdown eth1; sudo ifup eth1;"); //Reset network interface 'eth1'.


    //Create the local (this server's) socket:
    int servSocketDesc = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); //IPPROTO_TCP=6.

    //Create the local (this server's) end point for the connection:
    sockaddr_in* ptrServEndPoint = new sockaddr_in;         //Create an empty end-point structure.
    memset(ptrServEndPoint, 0, sizeof *ptrServEndPoint);    //Clear the memory block occupied by the end-point structure by setting it to all zeros.

    ptrServEndPoint->sin_family = AF_INET;                  //Specify the address format to use, in the end-point structure.
    ptrServEndPoint->sin_port = htons(50000);               //Specify 50000 as the port number for the local (this server's) end-point structure in network-byte order.
    inet_aton("172.17.1.20", &(ptrServEndPoint->sin_addr)); //Specify the IPv4 address for the local (this server's) end-point structure (which is network interface spring.net17) in network-byte order.

    //Bind the local (this server's) end point to the (this server's) socket:
    int servEndPointSize = sizeof *ptrServEndPoint;                     //Determine the size of the end-point structure in bytes.
    bind(servSocketDesc, (sockaddr*)ptrServEndPoint, servEndPointSize); //Bind the end-point structure to the socket.


    cout << endl << "STARTING tcpdump IN THE BACKGROUND. WAIT HERE..." << endl << endl;
    system("sudo tcpdump -l -i eth1 -U -vvv -tttt -X -s 0 \'tcp port 50000\' > a4q1capture.txt 2>&1 &"); //Start tcpdump and run it in the background.
    sleep(1);

//~~~~~~~~~~~~~~~~~~~~
//Cryptographic stuff:
    long double n = 23;  //public number.
    int q = 17;          //public number.
    int x_a = 5;         //server's private number.
    long double y_a = (unsigned long int)pow(n, x_a) % q; //public number; calculated here by the server.
    long double y_b;     //value to come from client.
    int k_a;             //value to be calculated by server.
    int N = 6;           //the number of schemes to choose from.
    int schemeSelection; //the selected scheme (which will be independently calculated by both sides).
    cout << "GIVEN n=" << n << ", q=" << q << ", and x_a=" << x_a << ", WE USE THE DIFFIE-HELLMAN ALGORITHM TO CALCULATE y_a=" << y_a << "." << endl << endl;
    cout << "NOW GO TO THE CLIENT MACHINE AND START THE CLIENT PROGRAM THERE." << endl;
    cout << "WHEN YOU NEXT RETURN HERE, PRESS 'Enter' TO RECEIVE y_b FROM THE CLIENT." << endl;
//~~~~~~~~~~~~~~~~~~~~


    //Listen for connection requests:
    listen(servSocketDesc, 2);

    //Accept connection requests:
    sockaddr_in* ptrClientEndPoint = new sockaddr_in;   //Create an empty end-point structure.
    int clientEndPointSize = sizeof *ptrClientEndPoint; //Determine the size of the end-point structure in bytes.
    int clientSocketDesc = accept(servSocketDesc, (sockaddr*)ptrClientEndPoint, (socklen_t*)&clientEndPointSize); //Use the accept() function to create a socket descriptor that represents the client's socket, and associate the client's socket with the server's socket to complete the connection.


//~~~~~~~~~~~~~~~~~~~~
//Cryptographic stuff:
    cin.get();

    //Let the client be the first to send some data, so prepare to receive the data:
    char buffer[200];          //Create some storage space to hold incoming and outgoing messages.
    int messageLength;         //Create a variable to record the length of incoming messages.
    messageLength = recv(clientSocketDesc, buffer, sizeof buffer, 0); //Retrieve the message from the TCP layer as soon as it arrives and store the message in our storage space.
    buffer[messageLength] = 0; //Indicate where in the 'buffer' array that the message ends.

    //Display the message from the client:
    cout << "THE CLIENT SENT: y_b=" << buffer << "." << endl;
    y_b = atof(buffer);        //Convert the string representation of the number into a floating-point number.

    //Calculate k_a:
    k_a = (unsigned long int)pow(y_b, x_a) % q;
    cout << "THE CALCULATED KEY IS: k_a=" << k_a << "." << endl;

    //Calculate the scheme selection:
    schemeSelection = k_a % N;
    cout << "THE SELECTED SCHEME IS: " << schemeSelection << "." << endl << endl;

    //Send y_a to the client:
    cout << "PRESS 'Enter' TO SEND y_a TO THE CLIENT." << endl;
    cin.get();
    sprintf(buffer, "%d", (int)y_a);                   //Convert the floating-point number to a string.
    send(clientSocketDesc, buffer, strlen(buffer), 0); //Send the information to the client.
    cout << "NOW GO TO THE CLIENT MACHINE." << endl;

    //Display the encrypted message from the client:
    cout << "WHEN YOU NEXT RETURN HERE, PRESS 'Enter' TO RECEIVE AN ENCRYPTED MESSAGE FROM THE CLIENT." << endl;
    cin.get();
    messageLength = recv(clientSocketDesc, buffer, sizeof buffer, 0); //Retrieve the message from the TCP layer as soon as it arrives and store the message in our storage space.
    buffer[messageLength] = 0;      //Indicate where in the 'buffer' array that the message ends.
    cout << "THIS IS THE ENCRYPTED MESSAGE SENT FROM THE CLIENT:" << endl << endl;
    cout << buffer << endl << endl; //Display the ciphertext

    //Decrypt the ciphertext and display it:
    cout << "PRESS 'Enter' TO DECRYPT THE CIPHERTEXT." << endl;
    cin.get();
    char* ptrTempString = scheme(schemeSelection, 1, buffer); //Decrypt the message.
    cout << "THIS IS THE DECRYPTED MESSAGE:" << endl << endl;
    cout << ptrTempString << endl << endl;                    //Display the plaintext.

    //Send an encrypted message to the client:
    strcpy(buffer, "Cryptography hurts my brain! It is killing my healthy brain cells! Please, no more!");
    cout << "PRESS 'Enter' TO ENCRYPT THE FOLLOWING MESSAGE AND SEND THE CIPHERTEXT TO THE CLIENT:" << endl << endl;
    cout << buffer << endl;
    cin.get();
    ptrTempString = scheme(schemeSelection, 0, buffer); //Encrypt the message.
    strcpy(buffer, ptrTempString);                      //Copy the ciphertext to the buffer for sending.
    send(clientSocketDesc, buffer, sizeof buffer, 0);   //Send the ciphertext.
    cout << "NOW GO TO THE CLIENT MACHINE." << endl;

    //End the server program:
    cout << "WHEN YOU NEXT RETURN HERE, PRESS 'Enter' TO TERMINATE THIS SERVER PROGRAM." << endl;
    cin.get();
//~~~~~~~~~~~~~~~~~~~~


    //Shut down the connection:
    shutdown(clientSocketDesc, SHUT_RDWR); //SHUT_RDWR=2.

    //Close the sockets:
    close(clientSocketDesc);
    close(servSocketDesc);

    //Free memory:
    delete ptrClientEndPoint;
    delete ptrServEndPoint;
    delete [] ptrTempString;

    //Terminate tcpdump:
    system("sudo pkill \"tcpdump\";");


    cout << "FINISHED!" << endl;
    cout << "THE CAPTURED DATA IS FOUND IN THE FILE \'a4q1capture.txt\', WHICH IS LOCATED IN THE SAME DIRECTORY AS THIS SERVER PROGRAM." << endl;

    return 0;
}