IMPORTANT: NEED TO USE sudo TO EXECUTE THESE PROGRAMS!


INSTRUCTIONS AND EXPLANATION FOR QUESTION 1:
============================================

The encryption/decryption program is a simulation of a rotor-based
encryption/descryption machine. This machine has:

m=67 symbols. The symbols are:
    ABCDEFGHIJKLMNOPQRSTUVWXYZ //26
    abcdefghijklmnopqrstuvwxyz //26
    0123456789                 //10
     ,.?!                      //5
                               //Total: 67

r=2 rotors. This program actually provides 3 different rotors to choose from,
but only two rotors are ever used at any a given time. In this program, each
rotor has a fixed one-to-one mapping, which means that a rotor and a
permutation are the same thing. Since there are three different rotors to
choose from, it implies that there are three different one-to-one mappings of
input pins to output pins (i.e. permutations) to choose from.

N=6 schemes. Since this program offers three different rotors to choose from,
there is a total of 3!=6 schemes to choose from.

Here are the instructions:

1) Copy the file 'server' to the 'spring' machine, and copy the file 'client'
   to the 'autumn' machine. The file 'server' is the server program and the
   file 'client' is the client program.

2) Using sudo, execute the 'server' program on the 'spring' machine. The
   server will start tcpdump in the background to capture incoming and
   outgoing transmissions.

   Then follow the instructions printed to screen. The first instruction that
   the server program will give you is to execute the 'client' program on the
   'autumn' machine. You should also use sudo to execute the client program.

   The client program will initiate the connection between client and server.
   Once the connection between client and server is established, these are the
   data that will be transmitted between client and server, and the sequence
   that they will be transmitted in:

   a) The client will start by calculating and sending y[b] to the server.

   b) The server will receive y[b] and use it to calculate the key and the
      scheme selection (k[a] and i, respectively).

   c) The server will calculate y[a] and send it to the client.

   d) The client will receive y[a] and use it to calculate the key and scheme
      selection (k[b] and i, respectively). k[a] and k[b] should be the same,
      and they are.

   e) The client will then encrypt this message (without the quotes):

          "If Alan Turing were alive today to see this attempt of mine to implement a cipher, he would probably not be too impressed."

      to get this ciphertext:

          cQVgHZdGZiBNpEF ,2wOa2gMiKwH6FrtmS!37lnTARsjvXGhuAOy,5yn BHLRrU8m8AIQPf1BMXTN 9.ixODzRDEmY683qZ1!zJXVRfZJqZLOB 9erTs6HN9Tt

   f) The client will then transmit the ciphertext to the server.

   g) The server will receive the ciphertext and decrypt it to get back the
      original plaintext.

   h) The server will then encrypt this message (without the quotes):

          "Cryptography hurts my brain! It is killing my healthy brain cells! Please, no more!"

      to get this ciphertext:

          ivaAAOMjHLQV.wXSXC5J65p3RsxW.5HtfO!1cde2RF 4plzh,SOucTqJw2gL!O2qnY.?pPbBMrX.ctqdinu

   i) The server will then transmit the ciphertext to the client.

   j) The client will receive the ciphertext and decrypt it to get back the
      original plaintext.

4) The server program will automatically terminate tcpdump once the server is
   finished executing. The captured output will be stored in the file
   'a4q1capture.txt', which is found in the same directory as the server
   program.
